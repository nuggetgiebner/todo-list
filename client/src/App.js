import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom'
import Navbar from './Components/Navbar'

function App() {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path='/'>
          
        </Route>
      </Switch>
    </div>
  );
}

export default App;
