import React from 'react'
import { render } from '@testing-library/react'
import Navbar from './Navbar'

test('<Navbar />', () => {
    const { container, debug, getByTestId } = render(<Navbar />)
    // debug()

    // Take a snapshot
    expect(container.firstChild).toMatchSnapshot()

    // Check for Navbar title
    const navbarTitle = getByTestId('Navbar-title')
    expect(navbarTitle.textContent).toBe('ToDo List')

    // Check for button with Login text
    const loginButton = getByTestId('Navbar-login')
    expect(loginButton.textContent).toBe('Login')
    expect(loginButton).toHaveProperty('type', 'button')

    // Check for button with Sign Up text
    const signupButton = getByTestId('Navbar-signup')
    expect(signupButton.textContent).toBe('Sign Up')
    expect(signupButton).toHaveProperty('type', 'button')
})